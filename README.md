[phpList](https://phplist.com/) for Newsletters.

# Volumes


# Environment Variables
## DOMAIN
Domain where the phplist instance is reachable.

## DB_HOST
Database host.

## DB_PORT
- default: 3306

Database port.

## DB_NAME
Database name.

## DB_USER
Database user.

## DB_PASSWORD
Password for the database user.

## MAIL_SMTP_HOST
SMTP host used for notification emails.

## MAIL_SMTP_PORT
- default: 587

SMTP host port.

## MAIL_SMTP_SECURITY
- default: tls

Connection security for the SMTP host.

## MAIL_SMTP_NAME
Username for SMTP authentication.

## MAIL_SMTP_PASSWORD
Password for SMTP authentication.

# Ports
- 80
