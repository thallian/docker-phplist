<?php

/*

* ==============================================================================================================
*
*
* The minimum requirements to get phpList working are in this file.
* If you are interested in tweaking more options, check out the config_extended.php file
* or visit http://resources.phplist.com/system/config
*
* ** NOTE: To use options from config_extended.php, you need to copy them to this file **
*
==============================================================================================================

*/

// what is your Mysql database server hostname
$database_host = '{{ getenv "DB_HOST" }}';

// what is the name of the database we are using
$database_name = '{{ getenv "DB_NAME" }}';

// what user has access to this database
$database_user = '{{ getenv "DB_USER" }}';

// and what is the password to login to control the database
$database_password = '{{ getenv "DB_PASSWORD" }}';

// the mysql server port number if not the default
$database_port = {{ getenv "DB_PORT" "3306" }};

// if you have an SMTP server, set it here. Otherwise it will use the normal php mail() function
//# if your SMTP server is called "smtp.mydomain.com" you enter this below like this:
//#
//#     define("PHPMAILERHOST",'smtp.mydomain.com');
define('PHPMAILERHOST', '{{ getenv "MAIL_SMTP_HOST"}}:{{ getenv "MAIL_SMTP_PORT" "587"}}');
$phpmailer_smtpuser = '{{ getenv "MAIL_SMTP_NAME" }}';
$phpmailer_smtppassword = '{{ getenv "MAIL_SMTP_PASSWORD" }}';

//# you can set this to send out via a different SMTP port
define('PHPMAILERPORT', {{ getenv "MAIL_SMTP_PORT" "587"}});
define("PHPMAILER_SECURE",'{{ getenv "MAIL_SMTP_SECURITY" "tls" }}');

$pageroot = '';

// if TEST is set to 1 (not 0) it will not actually send ANY messages, but display what it would have sent
// this is here, to make sure you edited the config file and mails are not sent "accidentally"
// on unmanaged systems

define('TEST', 0);

/*

==============================================================================================================
*
* Settings for handling bounces
*
* This section is OPTIONAL, and not necessary to send out mailings, but it is highly recommended to correctly
* set up bounce processing. Without processing of bounces your system will end up sending large amounts of
* unnecessary messages, which overloads your own server, the receiving servers and internet traffic as a whole
*
==============================================================================================================

*/

// Message envelope.

// Handling bounces. Check README.bounces for more info
// This can be 'pop' or 'mbox'
$bounce_protocol = 'mbox';

// set this to 0, if you set up a cron to download bounces regularly by using the
// commandline option. If this is 0, users cannot run the page from the web
// frontend. Read README.commandline to find out how to set it up on the
// commandline
define('MANUALLY_PROCESS_BOUNCES', 0);

// when the protocol is mbox specify this one
// it needs to be a local file in mbox format, accessible to your webserver user
$bounce_mailbox = '/var/lib/phplist/bounces/listbounces';

// set this to 0 if you want to keep your messages in the mailbox. this is potentially
// a problem, because bounces will be counted multiple times, so only do this if you are
// testing things.
$bounce_mailbox_purge = 1;

// set this to 0 if you want to keep unprocessed messages in the mailbox. Unprocessed
// messages are messages that could not be matched with a user in the system
// messages are still downloaded into phpList, so it is safe to delete them from
// the mailbox and view them in phpList
$bounce_mailbox_purge_unprocessed = 1;

// how many bounces in a row need to have occurred for a user to be marked unconfirmed
$bounce_unsubscribe_threshold = 5;

// choose the hash method for password
// check the extended config for more info
// in most cases, it is fine to leave this as it is
define('HASH_ALGO', 'sha256');

// If you set up your system to send the message automatically (from commandline),
// you can set this value to 0, so "Process Queue" will disappear from the site
// this will also stop users from loading the page on the web frontend, so you will
// have to make sure that you run the queue from the commandline
// check README.commandline how to do this
define('MANUALLY_PROCESS_QUEUE', 0);

define('HTTP_HOST','{{ getenv "DOMAIN" }}');


// Admin protocol
// similar to the above, if you need to force the admin pages on either http or https (eg when behind a
// proxy that prevents proper auto-detection), you can set it here
// define('ADMIN_PROTOCOL','https');
