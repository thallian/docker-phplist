FROM registry.gitlab.com/thallian/docker-php7-fpm:master

ENV FPMUSER nginx
ENV FPMGROUP nginx

ENV VERSION 3.3.1

RUN apk add --no-cache  \
    libressl \
    tar \
    nginx \
    php7 \
    php7-opcache \
    php7-apcu \
    php7-pgsql \
    php7-pdo_pgsql \
    php7-imap \
    php7-openssl \
    php7-mbstring \
    php7-session \
    php7-curl \
    php7-iconv \
    php7-json \
    php7-gettext \
    php7-xml \
    php7-simplexml \
    php7-gd \
    php7-mysqli

RUN mkdir /var/lib/phplist
RUN wget -qO- https://sourceforge.net/projects/phplist/files/phplist/$VERSION/phplist-$VERSION.tgz/download | tar xz -C /var/lib/phplist --strip 1

RUN mkdir /var/lib/phplist/bounces

RUN chown -R nginx:nginx /var/lib/phplist

RUN mkdir /run/nginx
RUN rm /etc/nginx/conf.d/default.conf

ADD /rootfs /

VOLUME /var/lib/phplist/bounces
